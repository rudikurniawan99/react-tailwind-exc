import React from 'react'
import { Navbar } from '../components/molecules'

const MainApp = () => {
  return (
    <>
      <Navbar/>
    </>
  )
}

export default MainApp
