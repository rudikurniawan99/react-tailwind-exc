import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import { LogoIcon } from '../../assets/images'
import { Menu } from '@headlessui/react'

const Navbar = () => {

  const [isOpen, setIsOpen] = useState(false)

  return (
    <div className="bg-transparent py-4 shadow-md">
      <div className="container mx-auto px-3">
        <div className="flex justify-between items-center">
          <div className="w-12 h-12">
            <Link>
              <img src={LogoIcon} className="w-full" alt="" />
            </Link>
          </div>
          <div className="lg:hidden w-10 h-10">
            <button
              onClick={() => setIsOpen(!isOpen)} 
            >
              <svg xmlns="http://www.w3.org/2000/svg" className="w-full focus:outline-none" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M4 6h16M4 12h16m-7 6h7" />
              </svg>
            </button>
          </div>
          <div className="hidden lg:block">
            <NavItems/>
          </div>
        </div>
        {isOpen && (
          <div className="py-2">
            <NavItems/>
          </div>
        )}
      </div>
    </div>
  )
}

const NavItems = () => {
  const items = [
    {
      name: 'Career',
      path: '/career'
    },
    {
      name: 'Galleries',
      path: '/galleries'
    },
    {
      name: 'About Us',
      path: '/about-us'
    }
  ]
  return (
    <div className="flex flex-col gap-y-4 lg:gap-y-0 lg:gap-x-8 lg:flex-row lg:items-center">
      <div className="relative">
        <Menu>
          <Menu.Button>
            {({ open }) =>(
              <div className={`inline-flex ${open && 'text-gray-700'}`}>
                Product
                <svg xmlns="http://www.w3.org/2000/svg" className="ml-1 h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                  <path strokeLinecap="round" strokeLinejoin="round" strokeWidth={2} d="M19 9l-7 7-7-7" />
                </svg>
              </div>
            )}
            
          </Menu.Button>
          <Menu.Items className="mt-2 lg:absolute lg:bg-white lg:top-8 lg:left-0 lg:w-max">
            <div className="p-2 lg:py-4 lg:px-0 rounded-md border">
              <div className="flex gap-y-3 lg:gap-y-0 flex-col">
                <Menu.Item
                  as='div'
                >
                  <Link 
                    className="flex items-center px-3 py-1 text-gray-700 hover:bg-gray-200 hover:text-gray-800 "
                  >
                    <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                      <path fillRule="evenodd" d="M3 5a2 2 0 012-2h10a2 2 0 012 2v8a2 2 0 01-2 2h-2.22l.123.489.804.804A1 1 0 0113 18H7a1 1 0 01-.707-1.707l.804-.804L7.22 15H5a2 2 0 01-2-2V5zm5.771 7H5V5h10v7H8.771z" clipRule="evenodd" />
                    </svg>
                    <span className="ml-2">Desktop Computer</span>
                  </Link>
                </Menu.Item>
                <Menu.Item
                  as='div'
                >
                  <Link
                    className="flex items-center px-3 py-1 text-gray-700 hover:bg-gray-200 hover:text-gray-800 "
                  >
                    <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                      <path fillRule="evenodd" d="M7 2a2 2 0 00-2 2v12a2 2 0 002 2h6a2 2 0 002-2V4a2 2 0 00-2-2H7zm3 14a1 1 0 100-2 1 1 0 000 2z" clipRule="evenodd" />
                    </svg>
                    <span className="ml-2">Smartphone</span>
                  </Link>
                </Menu.Item>
              </div>
            </div>
          </Menu.Items>
        </Menu>
      </div>
      {items.map(item => (
        <NavItem 
          name={item.name} 
          path={item.path}
        />
      ))}
      <button className="bg-blue-500 py-1 lg:py-2 lg:px-8 w-full lg:w-auto rounded-lg text-white hover:bg-blue-400">Login</button>
    </div>
  )
}

const NavItem = ({name, path}) => {
  return (
    <Link path={path} className="text-gray-900 hover:text-gray-700">{name}</Link>
  )
}

export default Navbar
